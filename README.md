# README #

Powershell scripts for IDOL management

### What is this repository for? ###

* Manage IDOL services for windows using powershell scripts.
* Version 1.0

### How do I get set up? ###

* Pre-requisites
1. All IDOL services must start as "**HPE**".

![windows-services.png](https://bitbucket.org/repo/Aq6kez/images/904230963-windows-services.png)