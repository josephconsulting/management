﻿function FuncCheckService{
     param($ServiceName)
     $arrService = Get-Service -Name $ServiceName
     if ($arrService.Status -ne "Running"){
         Start-Service $ServiceName
         Write-Host "Starting " $ServiceName " service" 
         " ---------------------- " 
         " Service is now started"
     }
     if ($arrService.Status -eq "running"){ 
        Write-Host "$ServiceName service is already started"
        Stop-Service $ServiceName
        Write-Host "Stopping " $ServiceName " service"
         " ---------------------- " 
         " Service is now stopped"
        Start-Service $ServiceName
         Write-Host "Starting " $ServiceName " service" 
         " ---------------------- " 
         " Service is now started"
     }
 }

$services = Get-Service | Where-Object {$_.Name -eq 'HPE-IDOLServer'}

foreach ($service in $services)
{
    FuncCHeckService($service.Name)
}

Get-Service | Where-Object {$_.Name -match 'HPE'} | select DisplayName, Status | Format-List