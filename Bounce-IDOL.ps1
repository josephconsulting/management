﻿$services = Get-Service | Where-Object {$_.Name -match 'HPE'}
foreach ($service in $services){
    Restart-Service $service.Name
}

Get-Service | Where-Object {$_.Name -match 'HPE'} | select DisplayName, Status | Format-List