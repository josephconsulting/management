﻿        function FuncCheckService{
     param($ServiceName)
     $arrService = Get-Service -Name $ServiceName
     
     if ($arrService.Status -eq "running"){ 
        Stop-Service $arrService.Name
     }
 }

$services = Get-Service | Where-Object {$_.Name -match 'HPE'}

foreach ($service in $services)
{
    FuncCHeckService($service.Name)
}


Get-Service | Where-Object {$_.Name -match 'HPE'} | select DisplayName, Status | Format-List