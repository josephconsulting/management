﻿function FuncCheckService{
     param($ServiceName)
     $arrService = Get-Service -Name $ServiceName
     if ($arrService.Status -ne "Running"){
         Start-Service $ServiceName
         Write-Host "Starting " $ServiceName " service" 
         " ---------------------- " 
         " Service is now started"
     }
     if ($arrService.Status -eq "running"){ 
        Write-Host "$ServiceName service is already started"
     }
 }

$services = Get-Service | Where-Object {$_.Name -match 'HPE'}

foreach ($service in $services)
{
    FuncCHeckService($service.Name)
}

Get-Service | Where-Object {$_.Name -match 'HPE'} | select DisplayName, Status | Format-List